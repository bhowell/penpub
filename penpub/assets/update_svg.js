function updateSvg(urlTail, svgEl) {
    console.log('updateSvg starting')
    function fetchSvg(url, callback) {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', url + '?' + Math.random().toString().substr(2), true)
        xhr.responseType = 'document'
        xhr.overrideMimeType('text/xml');
        xhr.onload = function () {
          if (xhr.readyState === xhr.DONE) {
            if (xhr.status === 200) {
              callback(xhr.responseXML);
            }
          }
        };
        xhr.send(null);
    }

    function getTime(el) {
      return parseFloat(el.getAttribute('data-timestamp'))
    }

    fetchSvg(urlTail, function (svgTail) {
      console.log('fetched', urlTail);
      var recentPaths = Array.from(svgTail.getElementsByTagName('svg')[0].children);

      var lastKnownPath = svgEl.children[svgEl.children.length-1]

      if (getTime(recentPaths[0]) > getTime(lastKnownPath)) {
          // No overlap, we may have missed some strokes. Reload whole thing.
          // TODO
          console.log('no overlap!')
      }
      else {
          console.log('recentPaths: ', recentPaths)
          recentPaths.filter(path => getTime(path)>getTime(lastKnownPath))
              .forEach(path => {
                  console.log('appending', path);
                  var newPath = document.createElementNS('http://www.w3.org/2000/svg','path')
                  newPath.setAttribute('d', path.getAttribute('d'))
                  newPath.setAttribute('data-timestamp', path.getAttribute('data-timestamp'))
                  svgEl.ownerDocument.importNode(path)
                  svgEl.appendChild(newPath)
              });
      }
    });
}
var svgEl = document.getElementsByTagName('svg')[0];
var urlTail = 'page' + page_id + '-tail.svg';
window.setInterval(() => updateSvg(urlTail, svgEl), 1000);
