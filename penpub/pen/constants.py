# -*- coding: utf-8 -*-
"""
Provides static values for pen commands and hardware params
"""

DEFAULT_PASSWORD ='0000'

# The (not so unique) id of the Serial Port Protocol
UUID ='00001101-0000-1000-8000-00805F9B34FB'

commands = {
    # Pen Status & Setting (0X0x)
    0x01: ('Pen Power Status', 'PEN→APP', 'Notify Pen is ready'),
    0x02: ('Pen power Status', 'APP→PEN', 'Acknowledge for “Pen Ready”'),
    0x03: ('PEN RTC Setting', 'APP→PEN', ''),
    0x04: ('PEN RTC Setting', 'PEN→APP', ''),
    0x05: ('Hovering Mode Setting', 'APP→PEN', ''),
    0x06: ('Hovering Mode Setting', 'PEN→APP', ''),
    0x07: ('Pen Calibration', 'APP→PEN', ''),
    0x08: ('Pen Calibration', 'PEN→APP', ''),
    0x09: ('Auto Power Off Timer', 'APP→PEN', ''),
    0x0A: ('Auto Power Off Timer', 'PEN→APP', ''),
    0x0B: ('Valid ID', 'APP→PEN', ''),
    0x0C: ('Valid ID', 'PEN→APP', ''),
    0x0D: ('Password Request', 'PEN→APP', ''),
    0x0E: ('Password Request', 'APP→PEN', ''),
    0x0F: ('Password Change', 'APP→PEN', ''),
    0x10: ('Password Change', 'PEN→APP', ''),
    # Stroke Data Transmission (0x1x)
    0x11: ('Stroke Data transmission', 'PEN→APP', ''),
    0x13: ('Pen Up', 'PEN→APP', 'Sending pen up status'),
    0x14: ('Pen Up Acknowledge', 'APP→PEN', 'Response to pen up status'),
    0x15: ('Data Transmission for ID Change(2)', 'PEN→APP', ''),
    0x16: ('Transmission (pen up/down)', 'PEN→APP', ''),
    # Pen Status Information Request & Settings (0X2x, 0X3x)
    0x21: ('Pen Information', 'APP→PEN', 'Request information for battery, memory,.'),
    0x22: ('Pen Information', 'PEN→APP', 'Acknowledge for pen information'),
    0x25: ('PEN Status 2', 'PEN→APP', ''),
    0x26: ('PEN Status', 'APP→PEN', ''),
    0x27: ('PEN Status', 'PEN→APP', ''),
    0x28: ('LCD color setting', 'APP→PEN', 'Request color value to be displayed on Pen’s LED'),
    0x29: ('LCD color setting', 'PEN→APP', 'Acknowledge for LCD color setting'),
    0x2A: ('Auto Power On', 'APP→PEN', 'Requesting Auto power on.'),
    0x2B: ('Auto Power On', 'PEN→APP', 'Acknowledge for Auto Power On'),
    0x2C: ('Force Sensor Sensitivity', 'APP→PEN', 'Requesting force sensor sensitivity configuration'),
    0x2D: ('Force Sensor Sensitivity', 'PEN→APP', 'Acknowledge'),
    0x2E: ('Beep On/Off', 'APP→PEN', 'Requesting Beep On/Off'),
    0x2F: ('Beep On/Off', 'PEN→APP', 'Acknowledge'),
    # Data Transmission (0X4x)
    0x41: ('File Information', 'PEN→APP', 'File information to be sent'),
    0x42: ('File Information', 'APP→PEN', 'Acknowledge'),
    0x43: ('File data Transmission', 'PEN→APP', 'Send File data to App'),
    0x44: ('File data Transmission', 'APP→PEN', 'Acknowledge'),
    0x45: ('Request List of stored data ID', 'APP→PEN', 'Request the Data list stored in the pen'),
    0x46: ('Request List of stored data ID', 'PEN→APP', 'Acknowledge'),
    0x47: ('File Transmission', 'APP→PEN', 'Request list data by note ID'),
    0x48: ('File Transmission', 'PEN→APP', 'Acknowledge'),
    0x49: ('File Size', 'PEN→APP', 'Send the size of requested file'),
    0x4A: ('File Deletion', 'APP→PEN', 'Delete by the unit of Section/Owner/ID'),
    0x4B: ('File Deletion', 'PEN→APP', 'Acknowledge'),
    # Upgrade (0X5x)
    0x51: ('PEN FW upgrade', 'APP→PEN', 'PEN SW upgrade'),
    0x52: ('PEN FW binary', 'PEN→APP', 'Request Firmware binary'),
    0x53: ('PEN FW binary', 'APP→PEN', 'Send Requested binary'),
    0x54: ('PEN status', 'PEN→APP', '???????'),
}

