import asyncio
from collections import defaultdict, namedtuple
import sys
import time
import types

import bluetooth

from .constants import commands, DEFAULT_PASSWORD, UUID
from ..utils import Event, log, warn
from .utils import bytes_to_int, int_to_bytes


# References:
# [BTAdt]: https://github.com/NeoSmartpen/AndroidSDK/blob/6b078e3d9c07cab3c6137a7adbaef07db4f1e2b1/NASDK/app/src/main/java/kr/neolab/sdk/pen/bluetooth/BTAdt.java

class PenException(Exception):
    def __init__(self, message):
        self.message = message

def decode_packet(packet):
    if len(packet) < 5:
        # Cannot contain a whole packet.
        return None, packet
    if packet[0] != 0xC0:
        raise PenException("First packet byte is not 0xC0 but {:02X}".format(packet[0]))
    cmd = packet[1]
    data_length = bytes_to_int(packet[2:4])
    packet_size = 4+data_length+1
    if len(packet) < packet_size:
        # Does not contain the whole packet.
        return None, packet
    else:
        if packet[4+data_length] != 0xC1:
            warn("Last byte is not 0xC1 but {}".format(packet[-1]))
        data = packet[4:4+data_length]
        remaining_data = packet[packet_size:]
        return (cmd, data), remaining_data

def encode_packet(cmd, data=None):
    if data == None: data = b''
    packet = (
        b'\xC0'
        + int_to_bytes(cmd, 1)
        + int_to_bytes(len(data), 2)
        + data
        + b'\xC1'
    )
    return packet

def is_correct_mac_address(address):
    # Neo Smartpen MAC address (from [BTAdt]#39)
    ALLOWED_MAC_PREFIX = '9C:7B:D2'
    DENIED_MAC_PREFIX = '9C:7B:D2:01'
    return (address.startswith(ALLOWED_MAC_PREFIX) and
            not address.startswith(DENIED_MAC_PREFIX))

class Pen:
    def __init__(self, bluetooth_mac=None, bluetooth_port=None):
        self.onPenUp = Event()
        self.onFileReceived = Event()
        self.onConfigured = Event()
        self.configured = False
        self.path = [] # current path coordinates [(x,y),...]

        if bluetooth_mac is None or bluetooth_port is None:
            bluetooth_mac, bluetooth_port = self.find_pen()
        self.bluetooth_mac = bluetooth_mac
        self.bluetooth_port = bluetooth_port
        self.connect(bluetooth_mac, bluetooth_port)

        self.responders = {
            0x01: self.handle_power_on_off,
            0x0d: self.handle_request_password,
            0x25: self.handle_settings,
            0x11: self.handle_stroke_data,
            0x15: self.handle_page_change,
            0x16: self.handle_up_down,
        }
        self.listeners = defaultdict(list)

    def find_pen(self):
        log("Searching for device..")
        # find bluetooth devices with serial port protocol
        services = bluetooth.find_service(uuid=UUID)
        log("Found %d services: %s" % (len(services), services))
        # [{'description': None,
        #  'host': '9C:7B:D2:17:9C:05',
        #  'name': 'Serial Port',
        #  'port': 5,
        #  'profiles': [],
        #  'protocol': 'RFCOMM',
        #  'provider': None,
        #  'service-classes': ['1101'],
        #  'service-id': None},
        # {'description': None,
        #  'host': '9C:7B:D2:18:5E:71',
        #  'name': 'Serial Port',
        #  'port': 5,
        #  'profiles': [],
        #  'protocol': 'RFCOMM',
        #  'provider': None,
        #  'service-classes': ['1101'],
        #  'service-id': None}]

        # filter for correct mac address
        services = list(filter(lambda service: is_correct_mac_address(service['host']), services))
        if len(services) == 0:
            raise PenException("Device not found :(")

        if len(services) > 1:
            log("Multiple devices found! Using first one.")
            log("Found addresses:" +', '.join(map(lambda service: service['host'], services)))

        host = services[0]['host']
        port = services[0]['port']
        log("Found device: host='{}'; port='{}'".format(host, port))
        return (host, port)

    def connect(self, host, port):
        sock=bluetooth.BluetoothSocket( bluetooth.RFCOMM )
        self.sock = sock
        log("Connecting to device..")
        try:
            sock.connect((host, port))
        except bluetooth.btcommon.BluetoothError:
            raise PenException("Could not connect to device. :(")

    def disconnect(self):
        if self.sock:
            self.sock.close()

    def send(self, cmd, data):
        packet = encode_packet(cmd, data)
        log("Sending cmd {:02x} ({}): {}".format(cmd, commands[cmd][0], packet.hex()))
        self.sock.send(packet)

    # This function does nothing, but creates a value that can be yielded to
    # be read by run_handler.
    def expect_packet(self, *cmds):
        return (self.expect_packet, cmds)

    def process_command(self, cmd, data):
        log('Processing Command: {:02X}, {}'.format(cmd, commands[cmd][0]))
        if self.listeners[cmd]:
            listener = self.listeners[cmd][0] # take the first listener, in case there are multiple (should not occur)
            (old_cmds, responder) = listener
            # Remove the listener from all commands it was attached to.
            for old_cmd in old_cmds:
                self.listeners[old_cmd].remove(listener)
            # Continue the responder.
            self.run_handler(responder, (cmd, data))
        elif cmd in self.responders:
            responder = self.responders[cmd]
            maybe_gen = responder(data)
            if isinstance(maybe_gen, types.GeneratorType):
                self.run_handler(maybe_gen)
        else:
            warn("Unhandled command: {:02x}, {}".format(cmd, commands[cmd][0]))

    # Continue a responder until it yields. If it yields expect_packet, enqueue
    # it to listen for the specified commands.
    def run_handler(self, responder, value=None):
        try:
            (func, value) = responder.send(value)
        except StopIteration:
            pass
        else:
            assert func == self.expect_packet
            new_cmds = value
            for new_cmd in new_cmds:
                self.listeners[new_cmd].append((new_cmds, responder))

    def handle_power_on_off(self, data):
        rtc_time = bytes_to_int(data[0:8])
        status = data[8]
        force_max = data[9]
        sw_ver = data[10:15]
        if status==1: # ON
            log("Ack'ing status..")
            self.send(0x02, data[0:8] + b'\x00')
        elif status==0: # OFF
            self.disconnect()
            raise PenException("Device turned off.")

    def handle_request_password(self, data):
        retry_count = data[0]
        reset_count = data[1]
        log("retry_count={}".format(retry_count))
        if retry_count==0:
            # Try default password
            log("Trying default password: %s" % DEFAULT_PASSWORD)
            self.send(0x0e, DEFAULT_PASSWORD.encode() + b'\x00')
        else:
            log("Not risking guessing the password (pen erases all data after the 10th wrong retry).")

    def handle_settings(self, data):
        log('handle_settings running')
        if not self.configured:
            # self.set_settings()
            yield from self.set_rtc()
            # self.set_sensitivity(0)
            yield from self.set_valid_id()
            log('Configured!')
            self.configured = True
            self.onConfigured.emit()
        else:
            pass

    def handle_stroke_data(self, data):
        x = bytes_to_int(data[1:3]) * 100 + data[5]
        y = bytes_to_int(data[3:5]) * 100 + data[6]
        dt = data[0]  # time delta from previous dot
        self.path.append((x,y))

    def handle_page_change(self, data):
        owner_id = bytes_to_int(data[0:4])
        note_id = bytes_to_int(data[4:8])
        page_id = bytes_to_int(data[8:12])
        self.current_page_id = page_id

    def handle_up_down(self, data):
        t = bytes_to_int(data[0:8]) / 1000.0
        if len(self.path) == 0: return
        self.onPenUp.emit(t, page_id=self.current_page_id, path=self.path)
        self.path = []

    def set_settings(self):
        self.send(0x26, [
            0xFF, 0xFF, 0xFF, 0xFF, # Time zone
            0, 0, 0, 0, 0, 0, 0, 0, # Time tick
            0, 0, 0, 0, # Colour
            0, # Auto power on
            0, # Acceleration mode
            0, # Hover mode
            0, # Beep
            0, 0, # Auto power off timeLong
            0xFF, 0xFF, # Pen pressure
            0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0, # reserved
        ])

    def set_rtc(self):
        rtc_setting = int_to_bytes(int(time.time()*1000), 8)
        self.send(0x03,
            rtc_setting +
            b'\x00\x00\x00\x00' # Timezone shift
        )
        cmd, data = yield self.expect_packet(0x04)
        failed = data[0] == 0x01
        if failed:
            raise PenException("Failed to set pen's real time clock")

    def set_sensitivity(self, sensitivity):
        if not sensitivity in range(5):
            raise PenException("Sensitivity should be in range 0~4 (lower = more sensitive)")
        self.send(0x2C, bytes_to_int(sensitivity) + b'\x00')

    def set_valid_id(self):
        log('set_valid_id running')
        # 0x03 = subscribe to live updates on any note(book)
        self.send(0x0B, b'\x03\x00' + b'\x00' * 40)
        cmd, data = yield self.expect_packet(0x0C)
        failed = data[0] == 0x00
        if failed:
            raise PenException('Pen refuses to give live reports.')

    async def listen(self):
        loop = asyncio.get_event_loop()
        # Wait for data in another thread, to not block our event loop
        data = await loop.run_in_executor(None, lambda: self.sock.recv(999999))
        return data

    async def incoming_packets(self):
        data_buffer = b''
        while True:
            try:
                new_data = await self.listen()
            except bluetooth.BluetoothError as err:
                raise PenException("Bluetooth error: {}".format(err))
            log("Received: {}".format(new_data.hex()))
            data_buffer += new_data
            while True:
                packet, data_buffer = decode_packet(data_buffer)
                if packet:
                    yield packet
                else:
                    break

    async def main_loop(self):
        log("Listening..")
        async for (cmd, data) in self.incoming_packets():
            self.process_command(cmd, data)

    def run(self, block=True):
        loop = asyncio.get_event_loop()
        self.onConfigured.subscribe(lambda: loop.call_soon(self.start_reading_files))
        loop.run_until_complete(self.main_loop())
        loop.close()

    def start_reading_files(self):
        self.run_handler(self.read_files())

    def list_folders(self):
        self.send(0x45, b'\x00')
        folders = []
        while True:
            cmd, data = yield self.expect_packet(0x46)
            is_last_packet = data[0] == 0x01
            owner_id = data[1:5]
            note_count = data[5]
            note_ids = [data[6+i*4:6+(i+1)*4] for i in range(note_count)]
            folders.append({
                "owner_id": owner_id,
                "note_ids": note_ids,
            })
            log('folder: owner_id: {}, note_ids: {}'.format(owner_id, note_ids))
            if is_last_packet:
                break
        # Our way of returning a value: tagging it with this function
        yield (self.list_folders, folders)

    def read_files(self):
        log('Reading files.')
        list_folders = self.list_folders()
        func, value = next(list_folders)
        while func != self.list_folders:
            result = yield func, value
            func, value = list_folders.send(result)
        folders = value

        log("{} folder(s) to read.".format(len(folders)))
        for folder in folders:
            log("Reading folder with these note ids: {}".format(folder["note_ids"]))

            if not folder["note_ids"]:
                # Either we are up to date, or password has not yet been given?
                log("No files to read in this folder!")
                continue

            for note_id in folder["note_ids"]:
                # Ask for files
                self.send(0x47, folder["owner_id"] + int_to_bytes(1, 1) + note_id)

                cmd, data = yield self.expect_packet(0x49)
                file_count = bytes_to_int(data[0:4])
                total_file_size = bytes_to_int(data[4:8])
                log("File count: {}, total size: {}".format(file_count, total_file_size))

                for file_nr in range(file_count):
                    cmd, data = yield self.expect_packet(0x41)
                    file_name = data[0:128].decode()
                    file_size = bytes_to_int(data[128:132])
                    packet_count = bytes_to_int(data[132:134])
                    packet_size = bytes_to_int(data[134:136])
                    log("File name: {}, file size: {}, packet count: {}, packet size: {}".format(file_name, file_size, packet_count, packet_size))

                    self.send(0x42, b'\x01\x00') # Ack file info with success

                    file_data = b''
                    for file_packet_index in range(packet_count):
                        cmd, data = yield self.expect_packet(0x43)
                        index = bytes_to_int(data[0:2])
                        checksum = data[2]
                        file_data += data[3:]
                        log('Done with one packet. Index: {}, checksum: {}, len(file_data): {}'.format(index, checksum, len(file_data)))

                        self.send(0x44, int_to_bytes(index, 2)) # Ack file data

                    log("Done with this file")
                    self.onFileReceived.emit(file_data)

                yield self.expect_packet(0x48) # Wait for ack for completion of file transfer.
                log("Done with all files of this note_id of this folder")
            log("Done with all files of this folder!")
        log("Done with all folders!")
