import io
import zipfile

from .utils import bytes_to_int, int_to_bytes

def parse_penfile(file_data):
    f = io.BytesIO(file_data)
    if zipfile.is_zipfile(f):
        zf = zipfile.ZipFile(f)
        all_lines = []
        for file_info in zf.filelist:
            all_lines += parse_penfile_data(zf.read(file_info.filename))
        return all_lines
    else:
        return parse_penfile_data(file_data)

def parse_penfile_data(file_data):
    lines = []

    header = file_data[-64:]
    body = file_data[:-64]

    section_id = header[9]
    owner_id = bytes_to_int(header[6:9])
    note_id = bytes_to_int(header[10:14])
    page_id = bytes_to_int(header[14:18])
    line_count = bytes_to_int(header[22:26])
    data_size = bytes_to_int(header[26:30])
    checksum = header[-1]

    assert len(body) == data_size

    while len(body) > 0:
        if body[0:2] == b'LN':
            pen_up_time = bytes_to_int(body[2:10]) / 1000.0
            pen_down_time = bytes_to_int(body[10:18]) / 1000.0
            dot_total = bytes_to_int(body[18:22])
            # color_bytes = bytes_to_int(body[23:27])
            line_checksum = body[27]

            line_data_size = 28 + (dot_total * 8)
            t = pen_down_time
            path = []
            for dot_index in range(28, line_data_size, 8):
                dot_data = body[dot_index:dot_index+8]
                dt = dot_data[0] / 1000.0
                x = bytes_to_int(dot_data[1:3]) * 100 + dot_data[5]
                y = bytes_to_int(dot_data[3:5]) * 100 + dot_data[6]
                force = dot_data[7]
                t += dt # unused
                path.append((x, y))

            lines.append((pen_up_time, page_id, path))
            body = body[line_data_size:]
        else:
            body = body[1:]

    return lines

# def main():
#     from pprint import pprint
#     with open('testfile.pen', 'rb') as f:
#         file_data = f.read()
#     lines = parse_penfile(file_data)
#     pprint(lines)
#
# if __name__ == '__main__':
#     main()
