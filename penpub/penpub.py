# -*- coding: utf-8 -*-
import argparse
import configparser
import os
import sys

from .pen import Pen, PenException
from .svg_writer import SvgWriter
from .pen.penfile_parser import parse_penfile
from .utils import Event
from .rsync import Rsync


def main():
    # Parse command line arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("--book", "-b",
        default="~/.penpub",
        help="Config file to read/write")
    parser.add_argument("--import-from-file")
    args = parser.parse_args()

    # Read/create configuration file
    config_dir = os.path.abspath(os.path.expanduser(args.book))
    config_path = os.path.join(config_dir, 'penpub.cfg')
    # Create config dir if needed
    if not os.path.exists(config_dir):
        os.makedirs(config_dir)
    config = configparser.ConfigParser()
    # Create minimal config file if absent
    if not os.path.exists(config_path):
        config.add_section("penpub")
        config["penpub"]["rsync_target"] = ''
        with open(config_path, "w") as config_file:
            config.write(config_file)
    else:
        config.read(config_path)

    # Read config
    bluetooth_mac = config["penpub"].get("bluetooth_mac", None)
    bluetooth_port = config["penpub"].getint("bluetooth_port", None)
    start_page = config["penpub"].getint("start_page", 1)
    end_page = config["penpub"].getint("end_page", None)
    viewbox = config["penpub"].get("viewbox", "480 360 5520 8900")

    book_dir = os.path.join(config_dir, 'pub')

    # Have pen output written to SVG files
    writer = SvgWriter(book_dir, start_page, end_page, viewbox)

    def run_parse_file(file_data):
        lines = parse_penfile(file_data)
        for line in lines:
            writer.onPenUp(*line)

    if args.import_from_file:
        with open(args.import_from_file, 'rb') as f:
            file_data = f.read()
        run_parse_file(file_data)
        return

    # Set up remote syncing if configured to
    rsync_target = config["penpub"].get("rsync_target")
    if rsync_target:
        rsync = Rsync(book_dir, rsync_target)
        # Trigger one rsync now, and then on every update.
        rsync.trigger()
        writer.onWrite.subscribe(rsync.trigger)
    else:
        rsync = None

    pen = None
    try:
        # Connect to the pen
        pen = Pen(bluetooth_mac, bluetooth_port)

        # Remember the bluetooth settings to connect faster next time.
        if bluetooth_mac is None or bluetooth_port is None:
            config["penpub"]["bluetooth_mac"] = pen.bluetooth_mac
            config["penpub"]["bluetooth_port"] = str(pen.bluetooth_port)
            with open(config_path, "w") as config_file:
                config.write(config_file)

        # Start the main loop to listen to the pen's scribbles.
        pen.onPenUp.subscribe(writer.onPenUp)

        # When we have read files from the pen's storage, parse them and pass the lines to the writer
        pen.onFileReceived.subscribe(run_parse_file)

        pen.run()
    except KeyboardInterrupt:
        pass
    except PenException as e:
        print(e)
    finally:
        if rsync: rsync.stop()
        if pen: pen.disconnect()

if __name__ == '__main__':
    main()
