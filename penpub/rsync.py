import os
from multiprocessing import Process, Event, Value
from ctypes import c_bool

def _syncProcess(source, target, dirtyFlag, quitFlag, event):
    # For rsync, the trailing slash matters.
    if not source.endswith('/'):
        source = source + '/'
    try:
        while True:
            event.wait()
            event.clear()
            if dirtyFlag.value:
                dirtyFlag.value = False
                os.system('rsync -ravz {} {}'.format(source, target))
            if quitFlag.value:
                quitFlag.value = False
                break
    except KeyboardInterrupt:
        pass

class Rsync:
    def __init__(self, source, target):
        self.dirtyFlag = Value(c_bool, False)
        self.quitFlag = Value(c_bool, False)
        self.event = Event()
        args = (source, target, self.dirtyFlag, self.quitFlag, self.event)
        self.p = Process(target=_syncProcess, args=args)
        self.p.start()

    def trigger(self):
        self.dirtyFlag.value = True
        self.event.set()

    def stop(self):
        self.quitFlag.value = True
        self.event.set()
        self.p.join()
