import os
import re
import shutil
import time

from mako.lookup import TemplateLookup

from .utils import Event, log


assets_source_dir = os.path.join(os.path.dirname(__file__), 'assets')

template_dir = os.path.join(os.path.dirname(__file__), 'templates')
templates = TemplateLookup(template_dir, input_encoding='utf-8', output_encoding='utf-8')
svg_template = templates.get_template('svg_template.svg')
svg_tail_template = templates.get_template('svg_tail_template.svg')
page_template = templates.get_template('page_template.html')
index_template = templates.get_template('index_template.html')


class SvgWriter:
    def __init__(self, book_dir, start_page, end_page, viewbox):
        self.onWrite = Event()
        self.start_page = start_page
        self.end_page = end_page
        self.book_dir = book_dir
        self.viewbox = viewbox

        if not os.path.exists(book_dir):
            os.makedirs(book_dir)

        assets_target_dir = os.path.join(book_dir, 'assets')
        if not os.path.exists(assets_target_dir):
            shutil.copytree(assets_source_dir, assets_target_dir)

        if start_page is not None and end_page is not None:
            self.create_pages(start_page, end_page)

    def create_pages(self, start_page, end_page):
        for page_id in range(start_page, end_page+1):
            svg_filepath = self.get_or_create_svg(page_id)
            with open(svg_filepath) as f:
                svg = f.read()
                self.write_html_page(page_id, svg)
        self.generate_overview()
        self.onWrite.emit()

    def generate_overview(self):
        overview_filename = 'all.html'
        overview_path = os.path.join(self.book_dir, overview_filename)
        if not self.start_page or not self.end_page:
            log('No start_page and end_page configured. Not generating all.html')
            return
        page_ids = range(self.start_page, self.end_page)
        with open(overview_path, "wb") as f:
            f.write(index_template.render(page_ids=page_ids))
        # If there is no index.html yet, make it a symlink to all.html
        index_path = os.path.join(self.book_dir, 'index.html')
        if not os.path.exists(index_path):
            os.symlink(overview_filename, index_path)

    def onPenUp(self, t, page_id, path):
        path_str = (
                '<path data-timestamp="%.3f" d="%s" />'
            ) % (
                t,
                'M %d %d ' % path[0]
                + ' '.join(map(lambda c: 'L %d %d' % c, path[1:]))
            )
        # flush!
        self.append_svg(page_id, path_str)

    def append_svg(self, page_id, path_str):
        svg_filepath = self.get_or_create_svg(page_id)
        with open(svg_filepath, "r") as f:
            svg = f.read()

        svg = svg[0:svg.rfind("</svg>")]
        svg += path_str+"\n" + "</svg>"

        with open(svg_filepath, "w") as f:
            f.write(svg)

        self.write_html_page(page_id, svg)

        # Make a file 'page123-tail.html' that contains the paths made in the
        # last minute
        now = time.time()
        def is_recent(path_line):
            m = re.search('data-timestamp="([\d.]+)"', path_line)
            if m:
                try:
                    path_date = float(m.groups()[0])
                    if path_date > (now - 60):
                        return True
                except:
                    pass
            return False
        # FIXME always leave the last non-recent value in the tail, to tell
        # the receiver what the base was (for checking if they missed something).
        recent_paths = filter(is_recent, svg.split("\n"))
        tail_filename = svg_filepath.split('.svg')[0]+'-tail.svg'
        with open(tail_filename, "wb") as f:
            tail_file_contents = svg_tail_template.render(paths="\n".join(recent_paths))
            f.write(tail_file_contents)

        self.onWrite.emit()

    def get_or_create_svg(self, page_id):
        svg_filename = 'page%d.svg' % page_id
        svg_filepath = os.path.join(self.book_dir, svg_filename)
        if not os.path.exists(svg_filepath):
            with open(svg_filepath, "wb") as f:
                f.write(svg_template.render(viewbox=self.viewbox))
        return svg_filepath

    def write_html_page(self, page_id, svg):
        html_filename = 'page%d.html' % page_id
        with open(os.path.join(self.book_dir, html_filename), "wb") as f:
            f.write(page_template.render(
                title="page %d" % page_id,
                svg=svg,
                page_id=page_id,
                start_page=self.start_page,
                end_page=self.end_page,
            ))
