
class Event(object):
    def __init__(self):
        self.listeners = []

    def subscribe(self, listener):
        self.listeners.append(listener)

    def emit(self, *args, **kwargs):
        for listener in self.listeners:
            listener(*args, **kwargs)

def log(s):
    print(s)

def warn(s):
    print("WARNING: {}".format(s))

