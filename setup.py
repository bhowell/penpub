"""
This is a setup.py script for PenPub

"""
from setuptools import setup, find_packages

VERSION = "0.1"

setup(name="penpub",
      packages = find_packages(),
      version = VERSION,
      entry_points={
          'console_scripts': [
              'penpub = penpub:main'
          ]
      },
      install_requires = [
          "mako",
          "pybluez",
      ],
      license = "GPLv3",
      url = "http://penpub.ink",
      package_data = {
          "penpub": ["templates/*.svg",
                      "templates/*.html",
                      "pub/assets/*.js",
                      "pub/assets/*.css"],
      }, 
      include_package_data=True,
      classifiers = [
        "Development Status :: 4 - Beta",
	"License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
        'Operating System :: POSIX',
        'Programming Language :: Python',
         ],
     )

